import Vue from 'vue'
import Vuex from 'vuex'
import db from '../config/firebase'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    loading: false,
    dataMateri: [],
    dataKategori: []
  },
  mutations: {
    updateLoading (state, payload) {
      state.loading = payload
    },
    updateDataMateri (state, payload) {
      state.dataMateri = payload
    },
    updateDataKategori (state, payload) {
      state.dataKategori = payload
    }
  },
  actions: {
    updateLoading (ctx, payload) {
      ctx.commit('updateLoading', payload)
    },
    async getDataRekomendasi (ctx) {
      ctx.dispatch('updateLoading', true)
      try {
        await db.collection('materi').orderBy('jumlah_dibaca', 'desc').limit(3).get()
          .then(snapshot => {
            let data = []
            snapshot.docs.map((doc, i) => {
              data[i] = {...doc.data(), id: doc.id}
            })
            ctx.commit('updateDataMateri', data)
          })
      } catch (e) {
        return Promise.reject(e)
      } finally {
        ctx.dispatch('updateLoading', false)
      }
    },
    async getDataMateri (ctx) {
      ctx.dispatch('updateLoading', true)
      try {
        await db.collection('materi').limit(3).get()
          .then(snapshot => {
            let data = []
            snapshot.docs.map((doc, i) => {
              data[i] = {...doc.data(), id: doc.id}
            })
            ctx.commit('updateDataMateri', data)
          })
      } catch (e) {
        return Promise.reject(e)
      } finally {
        ctx.dispatch('updateLoading', false)
      }
    },
    async getDataMateriByKategori (ctx, payload) {
      ctx.dispatch('updateLoading', true)
      try {
        await db.collection('materi').where('id_kategori', '==', payload).limit(3).get()
          .then(snapshot => {
            let data = []
            snapshot.docs.map((doc, i) => {
              data[i] = {...doc.data(), id: doc.id}
            })
            ctx.commit('updateDataMateri', data)
          })
      } catch (e) {
        return Promise.reject(e)
      } finally {
        ctx.dispatch('updateLoading', false)
      }
    },
    async searchingDataMateri (ctx, payload) {
      ctx.dispatch('updateLoading', true)
      try {
        await db.collection('materi').where('keyword', 'array-contains-any', payload.split(' ')).get()
          .then(snapshot => {
            if (snapshot.docs.length === 0) {
              ctx.dispatch('getDataRekomendasi')
            } else {
              let data = []
              snapshot.docs.map((doc, i) => {
                data[i] = {...doc.data(), id: doc.id}
              })
              ctx.commit('updateDataMateri', data)
            }
          })
      } catch (e) {
        return Promise.reject(e)
      } finally {
        ctx.dispatch('updateLoading', false)
      }
    },
    async getDataKategori (ctx) {
      ctx.dispatch('updateLoading', true)
      try {
        await db.collection('kategori').get()
          .then(snapshot => {
            let data = []
            snapshot.docs.map((doc, i) => {
              data[i] = {...doc.data(), id: doc.id}
            })
            ctx.commit('updateDataKategori', data)
          })
      } catch (e) {
        return Promise.reject(e)
      } finally {
        ctx.dispatch('updateLoading', false)
      }
    }
  },
  modules: {
  }
})
