import Vue from 'vue'
import VueRouter from 'vue-router'
import Search from '../views/Search'

Vue.use(VueRouter)

const routes = [
  {
    path: '/search/:id_kategori?',
    name: 'search',
    component: Search,
    props: true
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
