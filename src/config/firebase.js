import firebase from 'firebase/app'
// import "firebase/auth";
import "firebase/firestore";
// import "firebase/storage";

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
var firebaseConfig = {
  apiKey: "AIzaSyAZX4j701TeZPdH_Jh739wS11o6Cw8HW48",
  authDomain: "dib-school.firebaseapp.com",
  databaseURL: "https://dib-school.firebaseio.com",
  projectId: "dib-school",
  storageBucket: "dib-school.appspot.com",
  messagingSenderId: "1019457846834",
  appId: "1:1019457846834:web:5a97523d389e757798862b",
  measurementId: "G-YDF10XX7F5"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// Get a references to the firestore service
const db = firebase.firestore()

// Get a reference to the storage service, which is used to create references in your storage bucket
// const storage = firebase.storage();

// // Create a storage reference from our storage service
// const ref = storage.ref();

export default db